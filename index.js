// Bài 1
/**
- Đầu vào: 3 giá trị ngày,tháng, năm.
- Các bước xử lý: 
  * Gắn giá trị ngày, tháng, năm cho từng biến dayValue, monthValue, yearValue.
  * Xét ngày, tháng, năm có hợp lệ không.
  * Xét từng trường hợp đặc biệt của tháng 1, 2, 3, 8 khi tìm ngày hôm qua.
  * Xét từng trường hợp đặc biệt của tháng 2, 12 khi tìm ngày mai.
  * các trường hợp 30 ngày và 31 ngày cố định khác gộp chung.
- Đầu ra: Ngày trước và sau.
 */
function namNhuan(year) {
  if (year % 4 == 0 && year % 100 != 0) {
    return `N`;
  } else if (year % 4 == 0 && year % 100 == 0 && year % 400 == 0) {
    return `N`;
  } else {
    return `KN`;
  }
}
function thang1HomQua(day, year) {
  if (day == 1) {
    return `Ngày hôm qua là 31/12/${year - 1}`;
  } else {
    return `Ngày hôm qua là ${day - 1}/01/${year}`;
  }
}
function thang2HomQua(day, year) {
  if (namNhuan(year) == `KN` && day > 28) {
    return alert(
      ` Tháng 2 năm ${year} không có quá 28 ngày. Nhập lại ngày tối đa 28.`
    );
  } else if (namNhuan(year) == `N` && day > 29) {
    return alert(
      ` Tháng 2 năm ${year} không có quá 29 ngày. Nhập lại ngày tối đa 29.`
    );
  } else if (day == 1) {
    return ` Ngày hôm qua là 31/01/${year}`;
  } else {
    return `Ngày hôm qua là ${day - 1}/02/${year}`;
  }
}
function thang3HomQua(day, year) {
  if (day > 30) {
    return alert(`Vui lòng nhập ngày tối đa là 30`);
  } else if (day == 1 && namNhuan(year) == "N") {
    return `Ngày hôm qua là ngày 29/02/${year}`;
  } else if (day == 1 && namNhuan(year) == "KN") {
    return `Ngày hôm qua là ngày 28/02/${year}`;
  } else {
    return `Ngày hôm qua là ${day - 1}/02/${year}`;
  }
}
function thang31NgayHomQua(day, month, year) {
  if (day == 1) {
    return `Ngày hôm qua là 30/${month - 1}/${year}`;
  } else if (day == 1 && month == 8) {
    return `Ngày hôm qua là 31/07/${year}`;
  } else {
    return `Ngày hôm qua là ${day - 1}/${month}/${year}`;
  }
}
function thang30NgayHomQua(day, month, year) {
  if (day == 31) {
    return alert(
      `Tháng ${month} không có ngày 31. Vui lòng nhập tối đa là ngày 30!`
    );
  } else if (day == 1) {
    return `Ngày hôm qua là 31/${month - 1}/${year}`;
  } else {
    return `Ngày hôm qua là ${day - 1}/${month}/${year}`;
  }
}
function lastDay() {
  var dayValue = document.getElementById("dayValue").value * 1;
  var monthValue = document.getElementById("monthValue").value * 1;
  var yearValue = document.getElementById("yearValue").value * 1;
  if (
    dayValue < 1 ||
    dayValue > 31 ||
    monthValue > 12 ||
    monthValue < 1 ||
    yearValue < 1 ||
    Number.isInteger(monthValue) == false ||
    Number.isInteger(yearValue) == false ||
    Number.isInteger(dayValue) == false
  ) {
    alert(` Vui lòng nhập dữ liệu hợp lệ`);
  } else if (monthValue == 1) {
    document.getElementById("dayResult").innerHTML = thang1HomQua(
      dayValue,
      yearValue
    );
  } else if (monthValue == 3) {
    document.getElementById("dayResult").innerHTML = thang3HomQua(
      dayValue,
      yearValue
    );
  } else if (monthValue == 2) {
    document.getElementById("dayResult").innerHTML = thang2HomQua(
      dayValue,
      yearValue
    );
  } else if (
    monthValue == 5 ||
    monthValue == 7 ||
    monthValue == 8 ||
    monthValue == 10 ||
    monthValue == 12
  ) {
    document.getElementById("dayResult").innerHTML = thang31NgayHomQua(
      dayValue,
      monthValue,
      yearValue
    );
  } else {
    document.getElementById("dayResult").innerHTML = thang30NgayHomQua(
      dayValue,
      monthValue,
      yearValue
    );
  }
}

function thang2NgayMai(day, year) {
  if (namNhuan(year) == `KN` && day > 28) {
    return alert(
      ` Tháng 2 năm ${year} không có quá 28 ngày. Nhập lại ngày tối đa 28.`
    );
  } else if (namNhuan(year) == `N` && day > 29) {
    return alert(
      ` Tháng 2 năm ${year} không có quá 29 ngày. Nhập lại ngày tối đa 29.`
    );
  } else if (
    (namNhuan(year) == "N" && day == 29) ||
    (namNhuan(year) == "KN" && day == 28)
  ) {
    return ` Ngày mai là 01/03/${year}`;
  } else {
    return ` Ngày mai là ${day + 1}/02/${year}`;
  }
}
function thang12NgayMai(day, year) {
  if (day == 31) {
    return ` Ngày mai là 01/01/${year + 1}`;
  } else {
    return ` Ngày mai là ${day + 1}/12/${year}`;
  }
}
function thang31NgayNgayMai(day, month, year) {
  if (day == 31) {
    return `Ngày mai là 01/${month + 1}/${year}`;
  } else {
    return `Ngày mai là ${day + 1}/${month}/${year}`;
  }
}
function thang30NgayNgayMai(day, month, year) {
  if (day == 31) {
    return alert(
      ` Tháng ${month} không có ngày 31. Vui lòng nhập tối đa ngày 30!`
    );
  } else if (day == 30) {
    return `Ngày mai là 01/${month + 1}/${year}`;
  } else {
    return `Ngày mai qua là ${day + 1}/${month}/${year}`;
  }
}
function nextDay() {
  var dayValue = document.getElementById("dayValue").value * 1;
  var monthValue = document.getElementById("monthValue").value * 1;
  var yearValue = document.getElementById("yearValue").value * 1;
  if (
    dayValue < 1 ||
    dayValue > 31 ||
    monthValue > 12 ||
    monthValue < 1 ||
    yearValue < 1 ||
    Number.isInteger(monthValue) == false ||
    Number.isInteger(yearValue) == false ||
    Number.isInteger(dayValue) == false
  ) {
    alert(` Vui lòng nhập dữ liệu hợp lệ`);
  } else if (monthValue == 2) {
    document.getElementById("dayResult").innerHTML = thang2NgayMai(
      dayValue,
      yearValue
    );
  } else if (monthValue == 12) {
    document.getElementById("dayResult").innerHTML = thang12NgayMai(
      dayValue,
      yearValue
    );
  } else if (
    monthValue == 1 ||
    monthValue == 3 ||
    monthValue == 5 ||
    monthValue == 7 ||
    monthValue == 8 ||
    monthValue == 10
  ) {
    document.getElementById("dayResult").innerHTML = thang31NgayNgayMai(
      dayValue,
      monthValue,
      yearValue
    );
  } else {
    document.getElementById("dayResult").innerHTML = thang30NgayNgayMai(
      dayValue,
      monthValue,
      yearValue
    );
  }
}
// bài 2
/**
- Đầu vào: 2 giá trị tháng, năm.
- Các bước xử lí:
  * Gắn giá trị tháng, năm cho từng biến monthValue2, yearValue2.
  * Xét tháng, năm có hợp lệ không.
  * Xét năm nhuần và không nhuần để xét cho tháng 2.
- Đầu ra:  Tháng có bao nhiêu ngày.
 */
function leapYear() {
  var monthValue2 = document.getElementById("monthValue2").value * 1;
  var yearValue2 = document.getElementById("yearValue2").value * 1;
  var result;

  if (yearValue2 % 4 == 0 && yearValue2 % 100 != 0) {
    result = "N";
  } else if (
    yearValue2 % 4 == 0 &&
    yearValue2 % 100 == 0 &&
    yearValue2 % 400 == 0
  ) {
    result = "N";
  } else {
    result = "KN";
  }

  if (
    monthValue2 > 12 ||
    monthValue2 < 1 ||
    yearValue2 < 1 ||
    Number.isInteger(monthValue2) == false ||
    Number.isInteger(yearValue2) == false
  ) {
    alert(` Vui lòng nhập dữ liệu hợp lệ`);
  } else if (result == "N" && monthValue2 == 2) {
    document.getElementById(
      "leapYear"
    ).innerHTML = ` Tháng ${monthValue2} năm ${yearValue2} có 29 ngày`;
  } else if (result == "KN" && monthValue2 == 2) {
    document.getElementById(
      "leapYear"
    ).innerHTML = ` Tháng ${monthValue2} năm ${yearValue2} có 28 ngày`;
  } else if (
    monthValue2 == 1 ||
    monthValue2 == 3 ||
    monthValue2 == 5 ||
    monthValue2 == 7 ||
    monthValue2 == 8 ||
    monthValue2 == 10 ||
    monthValue2 == 12
  ) {
    document.getElementById(
      "leapYear"
    ).innerHTML = ` Tháng ${monthValue2} năm ${yearValue2} có 31 ngày`;
  } else {
    document.getElementById(
      "leapYear"
    ).innerHTML = ` Tháng ${monthValue2} năm ${yearValue2} có 30 ngày`;
  }
}
// Bài 3
/**
- Đầu vào: Số nguyên dương có 3 chữ số.
- Các bước xử lý: 
  * Gắn giá trị số nguyên dương cho biến.
  * Xét số có phải có 3 chữ số ( >=100 && <=999)và là số nguyên dương không.
  * tìm các hàng đơn vị, trăm, chục -> áp dụng hàm để chuyển chữ thành số.
  * xuất ra màn hình cách đọc.
- Đầu ra: cách đọc.
 */
function doiChu(a) {
  switch (a) {
    case 1:
      return "một";
    case 2:
      return "hai";
    case 3:
      return "ba";
    case 4:
      return "bốn";
    case 5:
      return "năm";
    case 6:
      return "sáu";
    case 7:
      return "bảy";
    case 8:
      return "tám";
    case 9:
      return "chín";
    case 0:
      return "không";
  }
}
function readNumber() {
  var threeNumber = document.getElementById("threeNumber").value * 1;
  if (threeNumber > 99 && threeNumber < 1000 && Number.isInteger(threeNumber)) {
    var hangTram = Math.floor(threeNumber / 100);
    console.log("🚀 ~ file: index.js:234 ~ readNumber ~ hangTram", hangTram);
    var hangChuc = Math.floor((threeNumber % 100) / 10);
    console.log("🚀 ~ file: index.js:235 ~ readNumber ~ hangChuc", hangChuc);
    var donVi = (threeNumber % 100) % 10;
    console.log("🚀 ~ file: index.js:236 ~ readNumber ~ donVi", donVi);
    var hangTramChu = doiChu(hangTram);
    console.log(
      "🚀 ~ file: index.js:237 ~ readNumber ~ hangTramChu",
      hangTramChu
    );
    var hangChucChu = doiChu(hangChuc);
    console.log(
      "🚀 ~ file: index.js:239 ~ readNumber ~ hangChucChu",
      hangChucChu
    );
    var donViChu = doiChu(donVi);
    console.log("🚀 ~ file: index.js:241 ~ readNumber ~ donViChu", donViChu);

    if (hangChuc == 0 && donVi == 0) {
      document.getElementById("readNumber").innerHTML = `${hangTramChu} trăm`;
    } else if (hangChuc == 0) {
      document.getElementById(
        "readNumber"
      ).innerHTML = `${hangTramChu} trăm lẻ ${donViChu}`;
    } else if (donVi == 0) {
      if (hangChuc == 1) {
        document.getElementById(
          "readNumber"
        ).innerHTML = `${hangTramChu} trăm mười`;
      } else {
        document.getElementById(
          "readNumber"
        ).innerHTML = `${hangTramChu} trăm ${hangChucChu} mươi`;
      }
    } else if (hangChuc == 1) {
      document.getElementById(
        "readNumber"
      ).innerHTML = `${hangTramChu} trăm mười ${donViChu}`;
    } else {
      document.getElementById(
        "readNumber"
      ).innerHTML = `${hangTramChu} trăm ${hangChucChu} mươi ${donViChu}`;
    }
  } else {
    alert("Số không hợp lệ");
  }
}
// Bài 4
/**
- Đầu vào:Tên các sinh viên và tọa độ, Tọa độ trường học.
- Các bước xử lý:
  * Gắn giá trị tên và các tọa độ cho biến.
  * Tạo một hàm function để tính khoảng cách từ nhà sinh viên đến trường.
  * So sánh khoảng cách -> tìm ra max.
- Đầu ra: Sinh viên có nhà xa nhất.
 */
function distance(a1, a2, s1, s2) {
  return Math.sqrt(Math.pow(s1 - a1, 2) + Math.pow(s2 - a2, 2));
}
function furthestSchool() {
  var nameA = document.getElementById("nameA").value;
  var nameB = document.getElementById("nameB").value;
  var nameC = document.getElementById("nameC").value;
  var xA = +document.getElementById("xA").value;
  var yA = +document.getElementById("yA").value;
  var xB = +document.getElementById("xB").value;
  var yB = +document.getElementById("yB").value;
  var xC = +document.getElementById("xC").value;
  var yC = +document.getElementById("yC").value;
  var xS = +document.getElementById("xS").value;
  var yS = +document.getElementById("yS").value;

  if (
    distance(xA, yA, xS, yS) >= distance(xB, yB, xS, yS) &&
    distance(xA, yA, xS, yS) >= distance(xC, yC, xS, yS)
  ) {
    document.getElementById(
      "furthestSchool"
    ).innerHTML = ` Bạn ${nameA} nhà xa trường nhất`;
  } else if (
    distance(xB, yB, xS, yS) >= distance(xA, yA, xS, yS) &&
    distance(xB, yB, xS, yS) >= distance(xC, yC, xS, yS)
  ) {
    document.getElementById(
      "furthestSchool"
    ).innerHTML = ` Bạn ${nameB} nhà xa trường nhất`;
  } else {
    document.getElementById(
      "furthestSchool"
    ).innerHTML = ` Bạn ${nameC} nhà xa trường nhất`;
  }
}
